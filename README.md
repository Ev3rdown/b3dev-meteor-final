For ttd, we run unit test for each commit where new code can be added (on feature, develop).
We run integration tests on develop.
When creating a new release, we run all test to make sure there is no problem before merging on master and deploying to prod.
When creating on hotfix, all tests have to be run because we don't create a new release before merging to master.


I created some "skip-x-test" commands, but have completely finished the logic behind, that would be way too much && || for readability.
We should basically be able to skip tests except on release and hotfixes


Also, merge pipeline shouldn't trigger anything as I don't use them, so I disabled tests on them.
I'm aware i could make something like "merge request for release must pass all tests" instead of "commit on release must pass all tests". (branch pipeline vs MR pipeline)
MR pipeline are probably more efficient in some cases as we don't always need to run tests on each commit

Environments:
- develop is optional, a developer can create a new deployment manualy if needs to. The new instance is created following these rules: environement="develop/COMMIT_SHORT_SHA" and url="COMMIT_SHORT_SHA.development.com"
- release is automatic, a new instance is created following these rules: environement="release/COMMIT_SHORT_SHA" and url="COMMIT_SHORT_SHA.release.com"
- production is automatic, environnement name is "production" and url is "cats.production.com" (because cats rule web)
When the environment is stopped, the instance is destroyed/deleted, except from production (of course ?).
